Feature: Verify Successful Registration and Menu Interaction

  Scenario: Successful Registration
	When OpenMainPage
	And FillData
	And MakeRegistrationSubmit
	Then VerifyData

  Scenario Outline: Menu Interaction
	Given OpenMainPage
	When Click "<href>"
	Then Become "<page>"

  	Examples:
   	| href	| page							|
   	| TISS  | https://tiss.tuwien.ac.at/  	|
   	| TUWEL | https://tuwel.tuwien.ac.at/ 	|






