package at.ac.tuwien.inso.swtesten.sample1;

import cucumber.api.java8.En;

public class WikipediaStepDefinitions implements En {

	private WikipediaSeleniumHelper wikiSeleniumHelper = new WikipediaSeleniumHelper();

	public WikipediaStepDefinitions() {

		Given("the browser language is \"([^\"]*)\"", (String locale) -> {
			wikiSeleniumHelper.setBrowserLanguage(locale);
		});

		Given("I am on the Wikipedia start page", () -> {
			wikiSeleniumHelper.setUp();
		});

		Given("the English version is selected", () -> {
			wikiSeleniumHelper.selectLanguage();
		});

		When("I enter the search term \"([^\"]*)\"", (String searchTerm) -> {
			wikiSeleniumHelper.search(searchTerm);
		});

		When("I enter the invalid search term \"([^\"]*)\"", (String invalidSearchTerm) -> {
			wikiSeleniumHelper.search(invalidSearchTerm);
		});

		Then("the result page with heading \"([^\"]*)\" is shown", (String heading) -> {
			wikiSeleniumHelper.assertSearchResult(heading);
			wikiSeleniumHelper.shutDown();
		});

		Then("no results are found and an error message \"([^\"]*)\" is displayed", (String heading) -> {
			wikiSeleniumHelper.assertArticleNotExists(heading);
			wikiSeleniumHelper.shutDown();
		});

	}
}
