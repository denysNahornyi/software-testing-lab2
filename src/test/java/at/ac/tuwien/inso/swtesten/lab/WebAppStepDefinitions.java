package at.ac.tuwien.inso.swtesten.lab;

import cucumber.api.PendingException;
import cucumber.api.java8.En;

public class WebAppStepDefinitions implements En{

	private WebAppSeleniumHelper webAppSeleniumHelper = new WebAppSeleniumHelper();

	public WebAppStepDefinitions() {

		Given("^OpenMainPage$", () -> {
			webAppSeleniumHelper.setUp();
		});
		And("^FillData$", () -> {
			webAppSeleniumHelper.dataFilling();
		});
		When("^MakeRegistrationSubmit$", () -> {
			webAppSeleniumHelper.registration();
		});
		Then("^VerifyData$", () -> {
			// Write code here that turns the phrase above into concrete actions
			webAppSeleniumHelper.dataVerification();
			webAppSeleniumHelper.shutDown();
		});
		When("^Click \"([^\"]*)\"$", (String href) -> {
			webAppSeleniumHelper.openPage(href);
		});
		Then("^Become \"([^\"]*)\"$", (String url) -> {
			webAppSeleniumHelper.checkCurrentPageUrl(url);
			webAppSeleniumHelper.shutDown();
		});
	}
}
