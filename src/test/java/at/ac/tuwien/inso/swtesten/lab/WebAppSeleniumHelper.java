package at.ac.tuwien.inso.swtesten.lab;

import at.ac.tuwien.inso.swtesten.util.SeleniumWebDriver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.containsString;

public class WebAppSeleniumHelper {

	private final String NAME = "Denys";
	private final String SECONDNAME = "Nahornyi";
	private final String BIRTHDAY = "09/12/1995";
	private final String GENDER = "male";
	private final String UNI = "technischeUniversitätWien";
	private final String STUDENT = "student";



	private WebDriver driver;
	private String baseUrl;

	public void setUp() {
		driver = SeleniumWebDriver.getDriver();
		baseUrl = "http://localhost:9090/lab-0.0.1-SNAPSHOT/";
		driver.get(baseUrl);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	public void dataFilling() {
		driver.findElement(By.name("firstname")).clear();
		driver.findElement(By.name("firstname")).sendKeys(NAME);

		driver.findElement(By.name("lastname")).clear();
		driver.findElement(By.name("lastname")).sendKeys(SECONDNAME);

		driver.findElement(By.name("birthday")).clear();
		driver.findElement(By.name("birthday")).sendKeys(BIRTHDAY);

		driver.findElement(By.cssSelector("input[value='"+GENDER+"']")).click();

		driver.findElement(By.cssSelector("option[value='"+UNI+"']")).click();

		driver.findElement(By.name(STUDENT)).click();

	}

	public void registration() {
		driver.findElement(By.cssSelector("button[value='true']")).click();
	}

	public void dataVerification() {
		Assert.assertThat(driver.findElement(By.className("a_name")).getText(), containsString(NAME+" "+SECONDNAME));
		Assert.assertThat(driver.findElement(By.className("a_birthday")).getText(), containsString(BIRTHDAY));
		Assert.assertThat(driver.findElement(By.className("a_gender")).getText(), containsString(GENDER));
		Assert.assertThat(driver.findElement(By.className("a_uni")).getText(), containsString(UNI));
		Assert.assertThat(driver.findElement(By.className("a_acknowledgements")).getText(), containsString(STUDENT));
	}

	public void openPage(String href) {
		driver.findElement(By.cssSelector("a[href='#']")).click();
		driver.findElement(By.xpath("//a[contains(text(),'"+href+"')]")).click();
	}

	public void checkCurrentPageUrl(String url) {
		Assert.assertEquals(driver.getCurrentUrl(),url);
	}

	public void shutDown() {
		SeleniumWebDriver.closeDriver();
	}


}
